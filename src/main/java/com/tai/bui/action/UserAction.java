package com.tai.bui.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.apache.struts.actions.MappingDispatchAction;

import com.tai.bui.dao.UserDao;
import com.tai.bui.model.User;
import com.tai.bui.service.UserService;

public class UserAction extends MappingDispatchAction {
	private UserService userService = new UserService();

	public ActionForward viewUser(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		int id = Integer.valueOf(request.getParameter("userId"));
		User user = userService.findUserById(id);
		request.setAttribute("user", user);
		return mapping.findForward("viewUser");
	}

	public ActionForward listUser(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		List<User> list = userService.findAllUser();
		request.setAttribute("userList", list);
		return mapping.findForward("listUser");
	}

	public ActionForward addUserPost(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		User user = (User) form;
		userService.addUser(user);
		return mapping.findForward("success");
	}

	public ActionForward addUser(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		return mapping.findForward("addUser");
	}

	public ActionForward deleteUser(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		int id = Integer.valueOf(request.getParameter("userId"));
		userService.deleteUserById(id);
		return mapping.findForward("success");
	}

	public ActionForward updateUser(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		int id = Integer.valueOf(request.getParameter("userId"));
		User user = userService.findUserById(id);
		request.setAttribute("user", user);
		return mapping.findForward("updateUser");
	}

	public ActionForward updateUserPost(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		User user = (User) form;
		userService.updateUser(user);
		return mapping.findForward("success");
	}

}
