package com.tai.bui.service;

import java.sql.SQLException;
import java.util.List;

import com.tai.bui.dao.UserDao;
import com.tai.bui.model.User;

public class UserService {
	
	private UserDao userDao = new UserDao();
	
	public List<User> findAllUser(){
		return userDao.findAll();
	}
	
	public User findUserById(int id) {
		return userDao.findById(id);
	}
	
	public void deleteUserById(int id) {
		userDao.delete(id);
	}
	
	public void updateUser(User user) {
		try {
			userDao.update(user);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void addUser(User user) {
		try {
			userDao.save(user);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
