package com.tai.bui.utils;

import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCConnection {
	public static java.sql.Connection getJDBCConnection() {
		final String url = "jdbc:mysql://localhost:3306/shop?autoReconnect=true&useSSL=false";
		final String user = "root";
		final String password = "admin";
		try {
			Class.forName("com.mysql.jdbc.Driver");
			return DriverManager.getConnection(url, user, password);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
