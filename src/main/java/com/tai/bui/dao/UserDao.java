package com.tai.bui.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.tai.bui.model.User;
import com.tai.bui.utils.JDBCConnection;


public class UserDao {
	public User findById(int Id) {
		String sql = "SELECT * FROM user WHERE ID = ?";
		User user = new User();
		Connection connection = JDBCConnection.getJDBCConnection();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			
			preparedStatement.setInt(1, Id);
			
			ResultSet rs =  preparedStatement.executeQuery();
			
			while(rs.next()) {
				user.setId(rs.getInt("ID"));
				user.setName(rs.getString("NAME"));
				user.setAge(rs.getInt("AGE"));
				user.setGender(rs.getString("GENDER"));
				user.setFavorite(rs.getString("FAVORITE"));
				user.setJob(rs.getString("JOB"));
				user.setRole(rs.getString("ROLE"));
				user.setAvatar(rs.getString("AVATAR"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}
	
	public List<User> findAll() {
		Connection connection = JDBCConnection.getJDBCConnection();
		String sql = "SELECT * FROM user";
		List<User> list = new ArrayList<User>();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			
			ResultSet rs =  preparedStatement.executeQuery();
			
			while(rs.next()) {
				User user = new User();
				user.setId(rs.getInt("ID"));
				user.setName(rs.getString("NAME"));
				user.setAge(rs.getInt("AGE"));
				user.setGender(rs.getString("GENDER"));
				user.setFavorite(rs.getString("FAVORITE"));
				user.setJob(rs.getString("JOB"));
				user.setRole(rs.getString("ROLE"));
				user.setAvatar(rs.getString("AVATAR"));
				list.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public void save(User user) throws java.sql.SQLException {
		Connection connection = JDBCConnection.getJDBCConnection();
		String sql = "INSERT INTO user(NAME, PASSWORD, AGE, GENDER, FAVORITE, JOB, ROLE, AVATAR) "
				+ "VALUE(?,?,?,?,?,?,?,?)";
		PreparedStatement preparedStatement = connection.prepareStatement(sql);
		preparedStatement.setString(1, user.getName());
		preparedStatement.setString(2, user.getPassword());
		preparedStatement.setInt(3, user.getAge());
		preparedStatement.setString(4, user.getGender());
		preparedStatement.setString(5, user.getFavorite());
		preparedStatement.setString(6, user.getJob());
		preparedStatement.setString(7, user.getRole());
		preparedStatement.setString(8, user.getAvatar());
		int rs = preparedStatement.executeUpdate();
		System.out.println(rs);
	}
	
	public void delete(int id) {
		Connection connection = JDBCConnection.getJDBCConnection();
		String sql = "DELETE FROM user WHERE ID = ?";
		int i = 0;
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			}
		
	}
	public void update(User user) throws java.sql.SQLException {
		Connection connection = JDBCConnection.getJDBCConnection();
		String sql = "UPDATE user SET NAME = ?, PASSWORD = ?, AGE = ?, GENDER = ?, FAVORITE = ?, "
				+ "JOB = ?, ROLE = ?, AVATAR = ? WHERE ID = " + user.getId();
		
		PreparedStatement preparedStatement = connection.prepareStatement(sql);
//		preparedStatement.setInt(1, user.getId());
		preparedStatement.setString(1, user.getName());
		preparedStatement.setString(2, user.getPassword());
		preparedStatement.setInt(3, user.getAge());
		preparedStatement.setString(4, user.getGender());
		preparedStatement.setString(5, user.getFavorite());
		preparedStatement.setString(6, user.getJob());
		preparedStatement.setString(7, user.getRole());
		preparedStatement.setString(8, user.getAvatar());
		int rs = preparedStatement.executeUpdate();
		System.out.println(rs);
	}
	
}
