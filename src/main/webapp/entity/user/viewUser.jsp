<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
	div {
		
	}
</style>
</head>
<body>
	<div>
		<p>
			<strong><bean:message key="user.name" />: </strong>
			<bean:write name="user" property="name" />
		</p>

		<p>
			<strong><bean:message key="user.age" />: </strong>
			<bean:write name="user" property="age" format="##" />
		</p>


		<p>
			<strong><bean:message key="user.gender" />: </strong>
			<bean:write name="user" property="gender" />
		</p>


		<p>
			<strong><bean:message key="user.favorite" />: </strong>
			<bean:write name="user" property="favorite" />
		</p>


		<p>
			<strong><bean:message key="user.job" />: </strong>
			<bean:write name="user" property="job" />
		</p>
	</div>


</body>
</html>