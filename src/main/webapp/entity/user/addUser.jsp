<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
form {
	padding: 10px 40px;
	max-width: 400px;
	margin-left: auto;
	margin-right: auto;
	margin-top: 30px;
	margin-bottom: 30px;
	box-shadow: 0 5px 6px 0 #9a9292;
	border: 1px solid;
}
form input {
	height: 30px;
    width: 100%;
    border-radius: 3px;
    outline: none;
    padding: 2px 5px;
    border: 1px solid #cdcdcd;
}
input[type="radio"], input[type="checkbox"] {
    width: 15px;
    height: 15px;
}
input[type="submit"] {
	margin: 20px auto;
}
select {
    height: 30px;
    width: 200px;
    border-radius: 3px;
    border: 1px solid #cdcdcd;
}
</style>
</head>
<body>
	<html:form action="/add-user-post.html" method="post">
		<p>
			<bean:message key="user.name" />
		</p>
		<html:messages id="name_error" property="user.name.required">
			<p style="color: red">
				<bean:write name="name_error" />
			</p>
		</html:messages>
		<html:text property="name" name="user"></html:text>

		<p>
			<bean:message key="user.password" />
		</p>
		<html:messages id="password_error" property="user.password.required">
			<p style="color: red">
				<bean:write name="password_error" />
			</p>
		</html:messages>
		<html:text property="password" name="user"></html:text>

		<p>
			<bean:message key="user.age" />
		</p>
		<html:messages id="age_error" property="user.age.required">
			<p style="color: red">
				<bean:write name="age_error" />
			</p>
		</html:messages>
		<html:text property="age" name="user"></html:text>
		<p>
			<bean:message key="user.gender" />
		</p>
		<html:radio property="gender" value="Male" name="user">
			<bean:message key="user.gender.male" />
		</html:radio>
		<html:radio property="gender" value="Female" name="user">
			<bean:message key="user.gender.female" />
		</html:radio>
		<p>
			<bean:message key="user.favorite" />
		</p>
		<html:checkbox property="favorite" name="user" value="Watching movie">
			<bean:message key="user.favorite.watchingmovie" />
		</html:checkbox>
		<html:checkbox property="favorite" name="user" value="Listen to music">
			<bean:message key="user.favorite.listentomusic" />
		</html:checkbox>
		<p>
			<bean:message key="user.role" />
		</p>
		<html:checkbox property="role" name="user" value="ROLE_USER">
			<bean:message key="user.role.user" />
		</html:checkbox>
		<html:checkbox property="role" name="user" value="ROLE_ADMIN">
			<bean:message key="user.role.admin" />
		</html:checkbox>
		<p>
			<bean:message key="user.job" />
		</p>
		<html:select property="job" name="user">
			<html:option value="Developer">
				<bean:message key="user.job.developer" />
			</html:option>
			<html:option value="Teacher">
				<bean:message key="user.job.teacher" />
			</html:option>
			<html:option value="Singer">
				<bean:message key="user.job.singer" />
			</html:option>
		</html:select>
		<html:submit>
			<bean:message key="button.submit" />
		</html:submit>
	</html:form>
</body>
</html>