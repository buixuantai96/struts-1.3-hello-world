<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
table {
	text-align: left;
}

table th {
	padding: 5px 10px;
	background-color: #f1f1f1;
}

table tr td {
	padding: 5px 10px;
	background-color: #c4c4c4;
	height: 35px;
}

table tr td a {
	border: 1px solid #a5a5a5;
	padding: 5px 7px;
	border-radius: 3px;
	background-color: #f9f9f9;
	text-decoration: none;
	color: black;
	box-shadow: 0px 1px 3px 0 #887e7e;
	transition: 0.35s ease-in-out;
	cursor: pointer;
}

table tr td a:hover {
	transition: 0.35s ease-in-out;
	box-shadow: none;
}

a {
	padding: 6px 10px;
	text-decoration: none;
	background-color: #4febf1;
	margin-bottom: 10px;
	display: inline-block;
	color: black;
	border-radius: 3px;
	border: 1px solid #16d216;
	transition: 0.35s;
}

a:hover {
	box-shadow: inset -60px 0px 0 0px #b9b9b9, inset 0 0 60px 0 #b9b9b9;
	transition: 0.35s;
}
</style>

</head>
<body>
	<html:link page="/add-user.html">
		<bean:message key="button.add" />
	</html:link>
	<logic:empty name="userList">
		<p>Khong co nguoi dung nao</p>
	</logic:empty>
	<logic:notEmpty name="userList">
		<table style="border: 1px solid black">
			<tr>
				<th><bean:message key="user.id" /></th>
				<th><bean:message key="user.name" /></th>
				<th><bean:message key="user.age" /></th>
				<th><bean:message key="user.gender" /></th>
				<th><bean:message key="user.favorite" /></th>
				<th><bean:message key="user.job" /></th>
				<th><bean:message key="user.role" /></th>
				<th><bean:message key="user.avatar" /></th>
				<th colspan="3"><bean:message key="option" /></th>
			</tr>
			<logic:iterate id="user" name="userList">
				<tr>
					<td><bean:write name="user" property="id" /></td>
					<td><bean:write name="user" property="name" /></td>
					<td><bean:write name="user" property="age" /></td>
					<td><bean:write name="user" property="gender" /></td>
					<td><bean:write name="user" property="favorite" /></td>
					<td><bean:write name="user" property="job" /></td>
					<td><logic:equal value="ROLE_ADMIN" name="user"
							property="role">Nguoi dung</logic:equal> <logic:equal
							value="ROLE_USER" name="user" property="role">ADMIN</logic:equal>
					</td>
					<td><bean:write name="user" property="avatar" /></td>
					<td><html:link page="/edit-user.html" paramId="userId"
							paramName="user" paramProperty="id">
							<bean:message key="button.edit" />
						</html:link></td>
					<td><html:link page="/delete-user.html" paramId="userId"
							paramName="user" paramProperty="id">
							<bean:message key="button.delete" />
						</html:link></td>
					<td><html:link page="/view-user.html" paramId="userId"
							paramName="user" paramProperty="id">
							<bean:message key="button.detail" />
						</html:link></td>
				</tr>
			</logic:iterate>
		</table>
	</logic:notEmpty>
</body>
</html>